﻿#include <iostream>

using namespace std;

class Animal
{
public:
	virtual void Voice()
	{
		cout << "What do the animals say?" << "\n" << "\n";
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		cout << "Dog said:" << "\t" << "Woof!" << "\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		cout << "Cat said:" << "\t" << "Meow!" << "\n";
	}
};

class Rat : public Animal
{
public:
	void Voice() override
	{
		cout << "Rat said:" << "\t" << "I didn't try to hide the last chocolate you!" << "\n";
	}
};

int main()
{
	cout << "Text check:" << "\n" << "\n";

	Animal A;
	A.Voice();

	Dog D;
	D.Voice();

	Cat C;
	C.Voice();

	Rat R;
	R.Voice();

	cout << "\n" << "\n" << "Array check:" << "\n" << "\n";

	Animal* arr[3]{new Dog, new Cat, new Rat};
	{
		for (int i = 0; i < 3; i++)
		{
			arr[i]->Voice();
		}
	}
	delete[] arr;
}